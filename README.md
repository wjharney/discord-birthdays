# Discord Birthdays Bot

Most Discord bots that have birthday reminders are user-focused, permitting one birthday per user. The goal of this bot is to allow users to manage multiple birthday reminders for multiple people, rather than just themselves.

This project has been abandoned, as its original target user base found a suitable alternative.