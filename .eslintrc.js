module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  ignorePatterns: [".eslintrc.js"],
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ["./tsconfig.json"]
  },
  plugins: [
    "jest",
    "@typescript-eslint",
  ],
  extends: [
    "eslint:recommended",
    "plugin:jest/recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:prettier/recommended",
  ],
  rules: {
    "no-console": "warn",
    // Enforced in tsconfig; lint rule is not necessary
    "@typescript-eslint/no-unused-vars": "off"
  },
  reportUnusedDisableDirectives: true
};
