import { merge } from "./merge";

describe("deep merge util", () => {
  it("deep merges objects", () => {
    const a = { a: 1, merged: { A: 1 } } as const;
    const b = { b: 2, merged: { B: 2 } } as const;
    const merged = merge<{ a: 1; b: 2; merged: { A: 1; B: 2 } }>(a, b);

    expect(merged).toEqual({ a: 1, b: 2, merged: { A: 1, B: 2 } });
    expect(merged).not.toBe(a);
    expect(merged).not.toBe(b);
    expect(merged.merged).not.toBe(a.merged);
    expect(merged.merged).not.toBe(b.merged);
  });
});
