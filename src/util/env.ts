export { config as loadEnv } from "dotenv";

/** Throws if the given key is not set on the given object */
export function ensureEnv<E extends NodeJS.ProcessEnv, K extends string>(
  env: E,
  key: K
): asserts env is E & Record<K, string> {
  if (!env[key]) throw new Error(`Missing environment variable: ${key}`);
}
