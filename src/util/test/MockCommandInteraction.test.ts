import { Client, CommandInteraction } from "discord.js";
import { MockCommandInteraction } from "./MockCommandInteraction";

describe("MockCommandInteraction", () => {
  const client = new Client({ intents: [] });

  it("can be created from interaction options", () => {
    const cmd = MockCommandInteraction.fromOptions([], client);
    expect(cmd).toBeInstanceOf(MockCommandInteraction);
  });

  it("can be created from partial interaction data", () => {
    const cmd = MockCommandInteraction.fromData({});
    expect(cmd).toBeInstanceOf(MockCommandInteraction);
  });

  it("can be created from partial raw interaction", () => {
    const cmd = new MockCommandInteraction({});
    expect(cmd).toBeInstanceOf(MockCommandInteraction);
  });

  it("can be created with custom values provided", () => {
    const cmd = new MockCommandInteraction();
    expect(cmd).toBeInstanceOf(MockCommandInteraction);
  });

  it("is a discord.js CommandInteraction", () => {
    const cmd = new MockCommandInteraction();
    expect(cmd).toBeInstanceOf(CommandInteraction);
  });

  it("has a mocked reply method", () => {
    const cmd = new MockCommandInteraction();
    expect(cmd.reply).toBeInstanceOf(jest.fn().constructor);
  });
});
