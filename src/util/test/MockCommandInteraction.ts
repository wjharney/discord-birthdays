import {
  APIApplicationCommandInteraction as Interaction,
  APIApplicationCommandInteractionDataOption as Option,
  APIChatInputApplicationCommandInteractionData as Data,
  ApplicationCommandType,
  InteractionType,
} from "discord-api-types/v9";
import {
  CacheType,
  Client,
  CommandInteraction,
  InteractionReplyOptions,
  SnowflakeUtil,
} from "discord.js";
import { merge } from "../merge";
import { DeepPartial } from "utility-types";

/**
 * Mock raw JSON from the Discord API, used to create a CommandInteraction.
 * @see https://discord.com/developers/docs/interactions/receiving-and-responding#interaction-object-interaction-structure
 */
export const MOCK_COMMAND_DATA: Interaction = {
  version: 1, // CONSTANT
  type: InteractionType.ApplicationCommand,
  token: "TOKEN",
  member: {
    user: {
      username: "USERNAME",
      public_flags: 0,
      id: SnowflakeUtil.generate(),
      discriminator: "0000",
      avatar: null,
    },
    roles: [],
    premium_since: null,
    permissions: "0",
    pending: false,
    nick: null,
    mute: false,
    joined_at: "2021-11-11T11:11:11.111111+00:00",
    deaf: false,
    avatar: null,
  },
  id: SnowflakeUtil.generate(),
  guild_id: SnowflakeUtil.generate(),
  data: {
    type: ApplicationCommandType.ChatInput,
    options: [] as Option[],
    id: SnowflakeUtil.generate(),
    name: "test-command",
  },
  channel_id: SnowflakeUtil.generate(),
  application_id: SnowflakeUtil.generate(),
};

// Prevent accidental modification of the base config
(function freeze(obj: unknown) {
  Object.freeze(obj);
  // This breaks on recursive structures, but it isn't one
  if (obj && typeof obj === "object") Object.values(obj).map(freeze);
})(MOCK_COMMAND_DATA);

/** Extension of CommandInteraction used for testing. */
export class MockCommandInteraction<
  Cached extends CacheType
> extends CommandInteraction<Cached> {
  static DEFAULT_CLIENT = new Client({ intents: [] });

  /**
   * Creates a mock CommandInteraction.
   * @param data Partial interaction data. Merged with default values so minimal info needs to be provided.
   * @param client Optional because we don't really need it.
   */
  constructor(
    data: DeepPartial<Interaction> = {},
    client = new.target.DEFAULT_CLIENT
  ) {
    super(client, merge<Interaction>(MOCK_COMMAND_DATA, data));
  }

  /** Creates a mock CommandInteraction from just interaction options data */
  static fromOptions<Cached extends CacheType>(
    options: Option[],
    client = this.DEFAULT_CLIENT
  ): MockCommandInteraction<Cached> {
    return new this(merge(MOCK_COMMAND_DATA, { data: { options } }), client);
  }

  /** Creates a mock CommandInteraction from just interaction `data` */
  static fromData<Cached extends CacheType>(
    data: DeepPartial<Data>,
    client = this.DEFAULT_CLIENT
  ): MockCommandInteraction<Cached> {
    return new this(merge(MOCK_COMMAND_DATA, { data }), client);
  }

  /**
   * The `reply` method is mocked by default because it's commonly used by methods under test.
   */
  // CommandInteraction#reply is overloaded, which breaks the types here, so we use `any` as a workaround. This is fine
  // because we only ever use the function signature that returns void.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  reply = jest.fn<Promise<any>, [InteractionReplyOptions]>();
}
