import { DeepPartial } from "utility-types";

/**
 * Deep merges objects. Returns a new object, rather than using the first argument as base.
 * @param args Objects to merge
 * @returns Merged object
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export function merge<T extends object>(...args: DeepPartial<T>[]): T {
  // Type assertion isn't true and we don't do anything to validate it...
  const target = {} as T;
  for (const arg of args) {
    // Type assertion because the type definition for Object.entries isn't perfect...
    const entries = Object.entries(arg) as Array<
      [keyof T, DeepPartial<T>[keyof DeepPartial<T>]]
    >;
    for (const [key, value] of entries) {
      const src = target[key];
      if (src == undefined || value === null || typeof value !== "object") {
        // src is unset or value is primitive, just assign it
        // Type assertion is fine for primitives, but not necessarily true for objects
        target[key] = value as unknown as T[keyof T];
      } else if (Array.isArray(value)) {
        // Concatenate arrays
        if (!Array.isArray(src)) {
          throw new Error(`Cannot merge array with ${typeof src}.`);
        }
        // Type assertion because T[keyof T] must be an array type, even though TS doesn't know
        target[key] = [...src, ...value] as unknown as T[keyof T];
      } else if (typeof src === "object" && typeof value === "object") {
        // Merge objects
        // Type assertions are all necessary because we T[keyof T] is an object, but TS doesn't
        const merged = merge(
          src as unknown as Record<string, unknown>,
          value as unknown as Record<string, unknown>
        );
        target[key] = merged as unknown as T[keyof T];
      } else {
        throw new Error(
          `Unknown behavior for ${typeof src} and ${typeof value}.`
        );
      }
    }
  }

  return target;
}
