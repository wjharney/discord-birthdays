/** Throws if the given key is not present on the given object */
// eslint-disable-next-line @typescript-eslint/ban-types
export function ensureKey<T extends object, K extends string>(
  obj: T,
  key: K
): asserts obj is T & Record<K, unknown> {
  if (!Object.prototype.hasOwnProperty.call(obj, key)) {
    throw new Error(
      `Expected key "${key}" in ${JSON.stringify(Object.keys(obj))}.`
    );
  }
}
