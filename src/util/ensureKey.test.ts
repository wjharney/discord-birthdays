import { ensureKey } from ".";

describe("ensureKey", () => {
  it("throws if the key is missing", () => {
    expect(() => ensureKey({ present: true }, "missing")).toThrow(
      new Error('Expected key "missing" in ["present"].')
    );
  });

  it("does not throw if the key is present", () => {
    expect(ensureKey({ present: true }, "present")).toBeUndefined();
  });
});
