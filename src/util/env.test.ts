import { ensureEnv } from ".";

describe("ensureEnv", () => {
  it("throws if the given property is missing", () => {
    expect(() => ensureEnv({}, "MISSING")).toThrow(
      "Missing environment variable: MISSING"
    );
  });

  it("throws if the property is present, but empty", () => {
    expect(() => ensureEnv({ EMPTY: "" }, "EMPTY")).toThrow(
      "Missing environment variable: EMPTY"
    );
  });

  it("does not throw if the given property is present", () => {
    expect(ensureEnv({ PRESENT: "YES" }, "PRESENT")).toBeUndefined();
  });
});
