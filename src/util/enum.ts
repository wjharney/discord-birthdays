export const isEnumValue = <V extends string>(
  str: string,
  enumish: Record<string, V>
): str is V => {
  const values: string[] = Object.values(enumish);
  return values.includes(str);
};
