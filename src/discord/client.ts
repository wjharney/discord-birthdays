import { Client, Intents } from "discord.js";
import logger from "loglevel";

import { onCommand } from "./commands";

export const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
client.once("ready", () => logger.info("Connected to Discord."));
client.on("interactionCreate", onCommand);
