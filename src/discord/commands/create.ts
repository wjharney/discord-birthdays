/**
 * Loads commands from this directory and makes a request to the Discord API to register them.
 */
import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import logger from "loglevel";

import { ensureEnv, loadEnv } from "../../util";
import { commands } from ".";

loadEnv();
ensureEnv(process.env, "DISCORD_CLIENT_ID");
ensureEnv(process.env, "DISCORD_GUILD_ID");
ensureEnv(process.env, "DISCORD_BOT_TOKEN");

new REST({ version: "9" })
  .setToken(process.env.DISCORD_BOT_TOKEN)
  .put(
    Routes.applicationGuildCommands(
      process.env.DISCORD_CLIENT_ID,
      process.env.DISCORD_GUILD_ID
    ),
    {
      body: commands.map((cmd) => cmd.data.toJSON()),
    }
  )
  .then(() => {
    const names = commands.map((cmd) => cmd.data.name);
    logger.info(`Successfully registered commands: ${names.join(", ")}`);
  })
  .catch((err) => logger.error(err));
