import { MockCommandInteraction } from "../../util/test";
import { route } from "./router";

describe("command router", () => {
  const getSubcommandMock = jest.fn();
  const getGroupMock = jest.fn();
  const groupHandler = jest.fn();
  const subcommandHandler = jest.fn();
  const command = new MockCommandInteraction();
  command.options.getSubcommand = getSubcommandMock;
  command.options.getSubcommandGroup = getGroupMock;
  const groups = new Map([["group", groupHandler]]);
  const subcommands = new Map([["subcommand", subcommandHandler]]);

  beforeEach(() => {
    command.reply.mockReset();
    getSubcommandMock.mockReset();
    getGroupMock.mockReset();
  });

  describe("with subcommand groups", () => {
    it("routes to a subcommand group", async () => {
      getGroupMock.mockImplementation(() => "group");
      await route(command, subcommands, groups);
      expect(groupHandler).toHaveBeenCalledWith(command);
    });

    it("replies with an error if group is not recognized", async () => {
      getGroupMock.mockImplementation(() => "missing");
      await route(command, subcommands, groups);
      expect(command.reply).toHaveBeenCalledWith({
        ephemeral: true,
        content: "Unrecognized subcommand group: missing",
      });
    });

    it("routes to a subcommand if no group specified", async () => {
      getSubcommandMock.mockImplementation(() => "subcommand");
      await route(command, subcommands, groups);
      expect(subcommandHandler).toHaveBeenCalledWith(command);
    });
  });

  describe("with just top-level subcommands", () => {
    it("routes to a subcommand", async () => {
      getSubcommandMock.mockImplementation(() => "subcommand");
      await route(command, subcommands);
      expect(subcommandHandler).toHaveBeenCalledWith(command);
    });

    it("replies with an error if subcommand is not recognized", async () => {
      getSubcommandMock.mockImplementation(() => "missing");
      await route(command, subcommands);
      expect(command.reply).toHaveBeenCalledWith({
        ephemeral: true,
        content: "Unrecognized subcommand: missing",
      });
    });

    it("replies with an error if no subcommand specified", async () => {
      await route(command, subcommands);
      expect(command.reply).toHaveBeenCalledWith({
        ephemeral: true,
        content: "Please specify a subcommand.",
      });
    });
  });
});
