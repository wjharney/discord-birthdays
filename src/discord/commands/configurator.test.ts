import { GuildConfig, init } from "../../db";
import { MockCommandInteraction } from "../../util/test";
import { createGetter, createSetter } from "./configurator";

describe("createGetter", () => {
  const getter = createGetter(
    GuildConfig,
    (cmd) => ({ guildId: cmd.guildId }),
    "birthdayReminderChannel",
    {
      onError: () => "error",
      onMissing: () => "missing",
      onSuccess: (channel) => `success: ${channel}`,
    }
  );

  beforeAll(async () => {
    await init("sqlite::memory", { logging: false });
    await GuildConfig.bulkCreate([
      { guildId: "full-entry", birthdayReminderChannel: "CHANNEL" },
      { guildId: "partial-entry" },
    ]);
  });

  afterAll(async () => await GuildConfig.sync({ force: true }));

  it("creates a getter function", () => {
    expect(typeof getter).toBe("function");
  });

  it("replies with the found value", async () => {
    const cmd = new MockCommandInteraction({ guild_id: "full-entry" });
    await getter(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: "success: CHANNEL",
    });
  });

  it("says a value is missing if a row is not found", async () => {
    const cmd = new MockCommandInteraction({ guild_id: "missing-entry" });
    await getter(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: "missing",
    });
  });

  it("says a value is missing if a row is missing the column", async () => {
    const cmd = new MockCommandInteraction({ guild_id: "partial-entry" });
    await getter(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: "missing",
    });
  });

  it("replies with an error when something breaks", async () => {
    const err = new Error("It broke.");
    GuildConfig.findOne = () => Promise.reject(err);

    const cmd = new MockCommandInteraction({ guild_id: "whatever" });
    await expect(getter(cmd)).rejects.toBe(err);
    expect(cmd.reply).toHaveBeenLastCalledWith({
      ephemeral: true,
      content: "error",
    });

    // Original method is inherited, not an own property of GuildConfig
    delete (GuildConfig as { findOne?: unknown }).findOne;
  });
});

describe("createSetter", () => {
  const GUILD = "GUILD";
  const CHANNEL = "CHANNEL";
  const cmd = new MockCommandInteraction({
    guild_id: "GUILD",
    user: { id: "USER" },
  });

  const getRow = async () =>
    await GuildConfig.findOne({ where: { guildId: GUILD } });

  const setter = createSetter(
    GuildConfig,
    (cmd) => ({ guildId: cmd.guildId }),
    "birthdayReminderChannel",
    {
      onError: (channel) => `error: ${String(channel)}`,
      onSuccess: (channel) => `success: ${String(channel)}`,
    }
  );

  beforeAll(async () => await init("sqlite::memory", { logging: false }));

  afterAll(async () => await GuildConfig.sync({ force: true }));

  beforeEach(async () => {
    const row = await getRow();
    row?.destroy();
    cmd.reply.mockClear();
  });

  it("creates a setter function", () => {
    expect(typeof setter).toBe("function");
  });

  it("stores the value", async () => {
    await setter(cmd, CHANNEL);
    const row = await getRow();
    expect(row).toBeInstanceOf(GuildConfig);
    expect(row?.getDataValue("birthdayReminderChannel")).toBe(CHANNEL);
  });

  it("tells the user that the channel has been updated", async () => {
    await setter(cmd, CHANNEL);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: `success: ${CHANNEL}`,
    });
  });

  it("tells the user if something goes wrong", async () => {
    const err = new Error("It broke.");
    GuildConfig.upsert = () => Promise.reject(err);

    await expect(setter(cmd, CHANNEL)).rejects.toBe(err);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: `error: ${CHANNEL}`,
    });

    // Original upsert is inherited, not on GuildConfig itself
    delete (GuildConfig as { upsert?: unknown }).upsert;
  });
});
