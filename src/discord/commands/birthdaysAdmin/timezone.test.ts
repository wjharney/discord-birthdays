import { toTimeOffset } from "./timezone";

describe("timezone offset formatter", () => {
  it("formats positive offsets", () => {
    expect(toTimeOffset(1)).toBe("+01:00");
  });

  it("formats negative offsets", () => {
    expect(toTimeOffset(-2)).toBe("-02:00");
  });

  it("formats zero offset", () => {
    expect(toTimeOffset(0)).toBe("+00:00");
  });

  it("formats fractional offsets", () => {
    expect(toTimeOffset(10.5)).toBe("+10:30");
    expect(toTimeOffset(-4.25)).toBe("-04:15");
  });
});
