import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";

import { GuildConfig } from "../../../db";
import { createGetter, createSetter } from "../configurator";

export const data = new SlashCommandSubcommandBuilder()
  .setName("enabled")
  .setDescription(
    "Manage whether reminders should be posted, or see current setting."
  )
  .addBooleanOption((opt) =>
    opt
      .setName("enabled")
      .setDescription("Whether to post reminders. Omit to see current setting.")
      .setRequired(false)
  );

const getEnabled = createGetter(
  GuildConfig,
  (cmd) => ({ guildId: cmd.guildId }),
  "birthdayRemindersEnabled",
  {
    onError: () =>
      "Birthday reminders are either enabled or disabled. (Something broke.)",
    onMissing: () => "Birthday reminders are currently enabled.",
    onSuccess: (enabled) =>
      `Birthday reminders are currently ${enabled ? "en" : "dis"}abled.`,
  }
);

const setEnabled = createSetter(
  GuildConfig,
  (cmd) => ({ guildId: cmd.guildId }),
  "birthdayRemindersEnabled",
  {
    onError: (enabled) =>
      `Something broke while ${
        enabled ? "en" : "dis"
      }abling birthday reminders.`,
    onSuccess: (enabled) =>
      `Birthday reminders are now ${enabled ? "en" : "dis"}abled.`,
  }
);

export const execute = async (cmd: CommandInteraction): Promise<void> => {
  const enabled = cmd.options.getBoolean("enabled");
  if (typeof enabled === "boolean") await setEnabled(cmd, enabled);
  else await getEnabled(cmd);
};
