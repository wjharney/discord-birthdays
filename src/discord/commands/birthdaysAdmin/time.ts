import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { GuildConfig } from "../../../db";
import { createGetter, createSetter } from "../configurator";

export const data = new SlashCommandSubcommandBuilder()
  .setName("time")
  .setDescription(
    "Manage when reminders should be posted, or see current setting."
  )
  .addStringOption((opt) =>
    opt
      .setName("time")
      .setDescription("Time of day to post the birthday reminders.")
      .setRequired(false)
  );

const pad = (num: number) => (num < 10 ? `0${num}` : `${num}`);

// Regex matches HH:mm(:ss)? (AM|PM)?, seconds are ignored
const timeRegex = /^([01]?\d|2[0-3]):([0-5]\d)(?::\d\d)?(?: ([ap]m))?$/i;
/**
 * Converts a time from an unknown format to 24h
 * @param time Time string in 12h or 24h format
 * @returns 24h time string or null for invalid input
 */
export const to24h = (time: string): string | null => {
  const match = timeRegex.exec(time.trim());
  if (!match) return null;

  let hour = Number(match[1]);
  const ampm = match[3]?.toLowerCase();
  // 15:00 AM is not valid
  if (hour > 12 && ampm === "am") return null;
  // 11:00 PM -> 23:00
  if (hour < 12 && ampm === "pm") hour += 12;
  // 12:00 AM -> 00:00
  if (hour === 12 && ampm === "am") hour = 0;
  const minute = match[2];
  // Missing minute is invalid
  return minute ? `${pad(hour)}:${minute}` : null;
};

/**
 * Converts a time from 24h format to HH:mm a
 * @param time Time in 24h format
 * @returns Time in 12h format
 */
export const to12h = (time: string): string => {
  const parts = time.trim().split(":");
  let hour = Number(parts[0]);
  const minute = parts[1] ?? "00";
  const ampm = hour < 12 ? "AM" : "PM";
  hour = hour % 12 || 12; // 13-23 => 1-11
  return `${hour}:${minute} ${ampm}`;
};

const getTime = createGetter(
  GuildConfig,
  (cmd) => ({ guildId: cmd.guildId }),
  "birthdayReminderTime",
  {
    onError: () => "Something broke while checking the time of day.",
    onMissing: () => "Birthday reminders are posted at noon.",
    onSuccess: (time) => `Birthday reminders are posted at ${to12h(time)}.`,
  }
);

const setTime = createSetter(
  GuildConfig,
  (cmd) => ({ guildId: cmd.guildId }),
  "birthdayReminderTime",
  {
    onError: (time) => `Failed to set time to ${String(time)}`,
    onSuccess: (time) =>
      `Birthday reminders will be now posted at ${
        time ? to12h(time) : "noon"
      }.`,
  }
);

export const execute = async (cmd: CommandInteraction): Promise<void> => {
  const time = cmd.options.getString("time");
  if (typeof time === "string") {
    const normalized = to24h(time);
    return normalized
      ? await setTime(cmd, normalized)
      : await cmd.reply({
          ephemeral: true,
          content: `Could not parse \`${time}\` to a valid time.`,
        });
  } else {
    return await getTime(cmd);
  }
};
