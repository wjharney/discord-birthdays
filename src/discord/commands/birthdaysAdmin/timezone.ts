import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { GuildConfig } from "../../../db";
import { createGetter, createSetter } from "../configurator";

export const data = new SlashCommandSubcommandBuilder()
  .setName("timezone")
  .setDescription(
    "Manage the time zone for birthday reminders, or see current settings."
  )
  .addNumberOption((opt) =>
    opt
      .setName("offset")
      .setDescription("UTC offset in hours. Omit to see current setting.")
      .setRequired(false)
  );

const pad = (num: number) => (num < 10 ? `0${num}` : `${num}`);
export const toTimeOffset = (tz: number): string => {
  const hour = Math.abs(tz | 0);
  const minute = Math.round(Math.abs(tz % 1) * 60);
  const sign = tz < 0 ? "-" : "+";
  return `${sign}${pad(hour)}:${pad(minute)}`;
};

const getTimezone = createGetter(
  GuildConfig,
  (cmd) => ({ guildId: cmd.guildId }),
  "birthdayReminderTimezone",
  {
    onError: () => "An error occurred while checking the timezone.",
    onMissing: () => "The current time zone offset is UTC+00:00.",
    onSuccess: (tz) => `The current time zone offset is UTC${toTimeOffset(tz)}`,
  }
);

const setTimezone = createSetter(
  GuildConfig,
  (cmd) => ({ guildId: cmd.guildId }),
  "birthdayReminderTimezone",
  {
    onError: (tz) => `Failed to set time zone to ${toTimeOffset(tz ?? 0)}.`,
    onSuccess: (tz) => `Time zone is now UTC${toTimeOffset(tz ?? 0)}`,
  }
);

export const execute = async (cmd: CommandInteraction): Promise<void> => {
  const tz = cmd.options.getNumber("offset");
  if (typeof tz === "number") return await setTimezone(cmd, tz);
  else return await getTimezone(cmd);
};
