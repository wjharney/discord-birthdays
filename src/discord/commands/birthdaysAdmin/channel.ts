import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";

import { GuildConfig } from "../../../db";
import { createGetter, createSetter } from "../configurator";

export const data = new SlashCommandSubcommandBuilder()
  .setName("channel")
  .setDescription(
    "Manage where reminders should be posted, or see current setting."
  )
  .addChannelOption((opt) =>
    opt
      .setName("channel")
      .setDescription(
        "Channel to post birthday reminders. Omit to see current setting."
      )
      .setRequired(false)
  );

const getChannel = createGetter(
  GuildConfig,
  (cmd) => ({ guildId: cmd.guildId }),
  "birthdayReminderChannel",
  {
    onError: () => "The channel for birthday reminders has not been set.",
    onMissing: () => "The channel for birthday reminders could not be found.",
    onSuccess: (channel) =>
      `Birthday reminders are currently posted in <#${channel}>.`,
  }
);

const setChannel = createSetter(
  GuildConfig,
  (cmd) => ({ guildId: cmd.guildId }),
  "birthdayReminderChannel",
  {
    onError: (channel) =>
      `Something went wrong while updating the channel${
        channel ? ` to <#${channel}>` : ""
      }.`,
    onSuccess: (channel) =>
      channel
        ? `Birthday reminders will now be sent to <#${channel}>.`
        : "How did you manage to set the birthday reminders channel to nothing?",
  }
);

export const execute = async (cmd: CommandInteraction): Promise<void> => {
  const channel = cmd.options.getChannel("channel");
  if (channel) return await setChannel(cmd, channel.id);
  else return await getChannel(cmd);
};
