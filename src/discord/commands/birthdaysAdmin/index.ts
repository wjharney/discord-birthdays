import {
  SlashCommandBuilder,
  SlashCommandSubcommandBuilder,
} from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { CommandData, createHandlerMap } from "../handler";
import { route } from "../router";

import * as channel from "./channel";
import * as enabled from "./enabled";
import * as time from "./time";
import * as timezone from "./timezone";

export const subcommands: CommandData<SlashCommandSubcommandBuilder>[] = [
  channel,
  enabled,
  time,
  timezone,
];

export const data = new SlashCommandBuilder()
  .setName("birthdays-admin")
  .setDescription("Server configuration for the /birthdays command");

subcommands.forEach((subcommand) => data.addSubcommand(subcommand.data));

const handlers = createHandlerMap(subcommands);

/** Base command handler for /birthdays-admin */
export const execute = async (cmd: CommandInteraction): Promise<void> => {
  if (!cmd.memberPermissions?.has("ADMINISTRATOR")) {
    return await cmd.reply({
      ephemeral: true,
      content: "You must have ADMINISTRATOR permissions to use this command.",
    });
  }
  await route(cmd, handlers);
};
