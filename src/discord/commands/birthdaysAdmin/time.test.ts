import { ApplicationCommandOptionType } from "discord-api-types/v9";
import { MockCommandInteraction } from "../../../util/test";
import { execute, to12h, to24h } from "./time";

describe("time to 24h format", () => {
  it("ignores seconds", () => {
    expect(to24h("12:34:56")).toBe("12:34");
  });

  it("converts AM to 24h", () => {
    expect(to24h("12:23 AM")).toBe("00:23");
    expect(to24h("12:23 am")).toBe("00:23");
  });

  it("converts PM to 24h", () => {
    expect(to24h("11:11 pm")).toBe("23:11");
    expect(to24h("11:11 PM")).toBe("23:11");
  });

  it("doesn't allow mixing AM and 24h", () => {
    expect(to24h("16:00 AM")).toBe(null);
  });

  it("pads single-digit hours", () => {
    expect(to24h("1:00")).toBe("01:00");
  });

  it("returns null for invalid input", () => {
    expect(to24h("99:9")).toBe(null);
  });
});

describe("time to 12h format", () => {
  it("converts AM times", () => {
    expect(to12h("01:00")).toBe("1:00 AM");
  });

  it("converts PM times", () => {
    expect(to12h("15:54")).toBe("3:54 PM");
  });

  it("converts noon correctly", () => {
    expect(to12h("12:00")).toBe("12:00 PM");
  });

  it("converts midnight correctly", () => {
    expect(to12h("00:00")).toBe("12:00 AM");
  });
});

describe("/birthdays-admin time command", () => {
  it("returns an error on invalid input", async () => {
    const cmd = MockCommandInteraction.fromOptions([
      {
        name: "time",
        value: "oops",
        type: ApplicationCommandOptionType.String,
      },
    ]);
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: "Could not parse `oops` to a valid time.",
    });
  });
});
