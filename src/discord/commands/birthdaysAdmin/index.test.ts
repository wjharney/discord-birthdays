import { MockCommandInteraction } from "../../../util/test";
import { execute } from ".";
import { Permissions } from "discord.js";

describe("/birthdays-admin command", () => {
  it("replies with an error without ADMINISTRATOR permissions", async () => {
    const cmd = new MockCommandInteraction();
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: "You must have ADMINISTRATOR permissions to use this command.",
    });
  });

  it("works normally with ADMINISTRATOR permissions", async () => {
    const cmd = new MockCommandInteraction({
      member: { permissions: Permissions.FLAGS.ADMINISTRATOR.toString() },
    });
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: "Please specify a subcommand.",
    });
  });
});
