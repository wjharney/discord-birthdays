import { CommandInteraction } from "discord.js";
import { CommandHandler } from "./handler";

/**
 * Routes a command to the correct subcommand handler
 * @param cmd discord.js command interaction
 * @param subcommands Map of subcommand names to handlers
 * @param groups Map of subcommand groups to handlers
 */
export async function route(
  cmd: CommandInteraction,
  subcommands: Map<string, CommandHandler>,
  groups?: Map<string, CommandHandler>
): Promise<void> {
  if (groups) {
    const group = cmd.options.getSubcommandGroup(false);
    if (group) {
      const handler = groups.get(group);
      return await (handler
        ? handler(cmd)
        : cmd.reply({
            ephemeral: true,
            content: `Unrecognized subcommand group: ${group}`,
          }));
    }
  }

  const subcommand = cmd.options.getSubcommand(false);
  if (subcommand) {
    const handler = subcommands.get(subcommand);
    return await (handler
      ? handler(cmd)
      : cmd.reply({
          ephemeral: true,
          content: `Unrecognized subcommand: ${subcommand}`,
        }));
  }

  return await cmd.reply({
    ephemeral: true,
    content: "Please specify a subcommand.",
  });
}
