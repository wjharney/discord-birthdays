import { SlashCommandBuilder } from "@discordjs/builders";
import logger from "loglevel";
import { MockCommandInteraction, MOCK_COMMAND_DATA } from "../../util/test";
import {
  CommandData,
  CommandHandler,
  createHandler,
  createHandlerMap,
} from "./handler";

const COMMAND: CommandData<SlashCommandBuilder> = {
  data: new SlashCommandBuilder().setName(MOCK_COMMAND_DATA.data.name),
  execute: jest.fn(),
};

describe("handler map creator", () => {
  it("creates a command map from a list of command exports", () => {
    const map = createHandlerMap([COMMAND]);
    expect(map.size).toBe(1);
    expect(map.get(COMMAND.data.name)).toBe(COMMAND.execute);
  });
});

describe("command handler creator", () => {
  it("creates a command handler from a command map", () => {
    const handler = createHandler(createHandlerMap([COMMAND]));
    expect(typeof handler).toBe("function");
    expect(handler).toHaveLength(1);
  });
});

describe("command handler", () => {
  let handler: CommandHandler;

  beforeAll(() => {
    handler = createHandler(createHandlerMap([COMMAND]));
    logger.setLevel(logger.levels.SILENT);
  });

  it("does nothing for interactions that are not commands", async () => {
    const interaction = new MockCommandInteraction();
    // Easier than creating a new mock class just for this
    interaction.isCommand = () => false;
    await expect(handler(interaction)).resolves.toBeUndefined();
    expect(COMMAND.execute).not.toHaveBeenCalled();
  });

  it("handles known commands", async () => {
    const interaction = new MockCommandInteraction();
    await expect(handler(interaction)).resolves.toBeUndefined();
    expect(COMMAND.execute).toHaveBeenCalledWith(interaction);
  });

  it("warns on unknown commands", async () => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    const loggerSpy = jest.spyOn(logger, "warn").mockImplementation(() => {});
    const interaction = MockCommandInteraction.fromData({ name: "missing" });
    await expect(handler(interaction)).resolves.toBeUndefined();
    expect(loggerSpy).toHaveBeenCalledWith("Unrecognized command: missing");
    loggerSpy.mockRestore();
  });

  it("only handles commands in guilds", async () => {
    const interaction = new MockCommandInteraction({ guild_id: undefined });
    await expect(handler(interaction)).resolves.toBeUndefined();
    expect(interaction.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: `You must be in a guild to use the command \`${COMMAND.data.name}\``,
    });
  });
});
