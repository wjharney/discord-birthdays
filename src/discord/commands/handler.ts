import type { CommandInteraction, Interaction } from "discord.js";
import logger from "loglevel";

/** Function that does something with a command interaction */
export type CommandHandler = (cmd: CommandInteraction) => Promise<void>;

/**
 * Interface expected to be exported from command handler files
 */
export type CommandData<Builder> = {
  /** The actual command handler */
  execute: CommandHandler;
  /** Data used to create the command */
  data: Builder;
};

/**
 * Converts an array of command file exports into a map that can get command handlers by name
 * @param commands Array of command file exports
 * @returns Map of command names to handlers
 */
export const createHandlerMap = (
  commands: CommandData<{ name: string }>[]
): Map<string, CommandHandler> =>
  new Map(commands.map((cmd) => [cmd.data.name, cmd.execute]));

/**
 * Creates a handler that validates that an interaction is a known command in a guild, and then passes the interaction
 * to that command's handler.
 * @param handlers Map of command names to handlers
 */
export const createHandler = (handlers: Map<string, CommandHandler>) =>
  async function onInteraction(interaction: Interaction): Promise<void> {
    if (!interaction.isCommand()) return;
    logger.info(
      `Command from ${interaction.user.tag} ((${interaction.user.id}) in ${
        interaction.guild?.name ?? "???"
      } (${interaction.guildId}): ${interaction.toString()}`
    );
    if (!interaction.inGuild()) {
      return await interaction.reply({
        ephemeral: true,
        content: `You must be in a guild to use the command \`${interaction.commandName}\``,
      });
    }

    const execute = handlers.get(interaction.commandName);
    if (execute) return await execute(interaction);
    else logger.warn(`Unrecognized command: ${interaction.commandName}`);
  };
