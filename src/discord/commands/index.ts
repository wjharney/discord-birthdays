import type { SlashCommandBuilder } from "@discordjs/builders";
import { CommandData, createHandler, createHandlerMap } from "./handler";

import * as birthdays from "./birthdays";
import * as birthdaysAdmin from "./birthdaysAdmin";

export const commands: CommandData<SlashCommandBuilder>[] = [
  birthdays,
  birthdaysAdmin,
];

export const onCommand = createHandler(createHandlerMap(commands));
