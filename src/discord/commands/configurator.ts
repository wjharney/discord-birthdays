import { CommandInteraction } from "discord.js";
import {
  FindOptions,
  Model,
  UpsertOptions,
  WhereOptions,
} from "sequelize/types";

/**
 * The `ModelCtor` type provided by sequelize uses `typeof Model`, which implicitly sets the type params to <any, any>.
 * This causes issues, so we re-implement what we need from the interface in a way that is actually usable.
 */
interface MinimalModelStatic<M extends Model> {
  prototype: M;
  findOne: (options: FindOptions<M["_attributes"]>) => Promise<M | null>;
  upsert: (
    values: M["_creationAttributes"],
    options?: UpsertOptions<M["_attributes"]>
  ) => Promise<[M, boolean | null]>;
}

type CreationAttributes<ModelStatic extends MinimalModelStatic<Model>> =
  ModelStatic["prototype"]["_creationAttributes"];

type ModelValues<
  ModelStatic extends MinimalModelStatic<Model>,
  K extends keyof CreationAttributes<ModelStatic>
> = CreationAttributes<ModelStatic>[K];

/** Messages to send based on the outcome of getting a value */
interface GetterMessages<
  ModelStatic extends MinimalModelStatic<Model>,
  K extends keyof CreationAttributes<ModelStatic>
> {
  /** When an error occurs */
  onError: () => string;
  /** When the value has not been set */
  onMissing: () => string;
  /** When the value has been set and found */
  onSuccess: (v: NonNullable<ModelValues<ModelStatic, K>>) => string;
}

/**
 * Creates a function that replies looks up the given column and replies with the value found
 * @param Model Sequelize model
 * @param getWhereOptions Method that returns a sequelize where clause
 * @param columnName Column to look up
 * @param messages Result message generators
 * @returns Function that replies with the value of the given column
 */
export function createGetter<
  ModelStatic extends MinimalModelStatic<Model>,
  K extends Exclude<
    keyof ModelStatic["prototype"]["_attributes"],
    number | symbol
  >
>(
  Model: ModelStatic,
  getWhereOptions: (
    cmd: CommandInteraction
  ) => WhereOptions<CreationAttributes<ModelStatic>>,
  columnName: K,
  messages: GetterMessages<ModelStatic, K>
) {
  /** Command handler that replies with the requested configuration value */
  return async function getConfig(cmd: CommandInteraction): Promise<void> {
    let response = messages.onError();
    try {
      const row = await Model.findOne({
        where: getWhereOptions(cmd),
        attributes: [columnName],
      });
      // This has to be `any` because that's the default for Model
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const value = row?.getDataValue(columnName);
      response =
        value == null ? messages.onMissing() : messages.onSuccess(value);
    } finally {
      await cmd.reply({
        ephemeral: true,
        content: response,
      });
    }
  };
}

/** Messages to send based on the outcome of setting a value */
interface SetterMessages<
  ModelStatic extends MinimalModelStatic<Model>,
  K extends keyof CreationAttributes<ModelStatic>
> {
  /** When an error occurs */
  onError: (v: ModelValues<ModelStatic, K>) => string;
  /** When the value is set successfully */
  onSuccess: (v: ModelValues<ModelStatic, K>) => string;
}

/**
 * Creates a function that updates a database with the given value and replies with the result
 * @param Model Sequelize model
 * @param getUpsertBase Method that returns a sequelize upsert clause
 * @param columnName Column to update
 * @param messages Result message generators
 * @returns Function that updates the database
 */
export function createSetter<
  ModelStatic extends MinimalModelStatic<Model>,
  K extends keyof CreationAttributes<ModelStatic>
>(
  Model: ModelStatic,
  getUpsertBase: (cmd: CommandInteraction) => CreationAttributes<ModelStatic>,
  columnName: K,
  messages: SetterMessages<ModelStatic, K>
) {
  return async function setConfig(
    cmd: CommandInteraction,
    columnValue: ModelValues<ModelStatic, K>
  ): Promise<void> {
    let response = messages.onError(columnValue);
    try {
      await Model.upsert({
        ...getUpsertBase(cmd),
        [columnName]: columnValue,
      });
      response = messages.onSuccess(columnValue);
    } finally {
      await cmd.reply({
        ephemeral: true,
        content: response,
      });
    }
  };
}
