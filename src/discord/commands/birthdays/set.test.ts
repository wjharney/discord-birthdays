import { CommandInteraction } from "discord.js";
import { Birthday, init } from "../../../db";
import { MockCommandInteraction } from "../../../util/test";
import { execute, parseDate } from "./set";

/** Basically new Date(), but with 1-indexed month */
const toDate = (year: number, month: number, day: number): Date =>
  new Date(Number(year), Number(month) - 1, Number(day));

describe("date parser", () => {
  const testValid = (date: Date, formats: string[]) =>
    formats.forEach((fmt) =>
      it(`parses ${fmt} as valid`, () => expect(parseDate(fmt)).toEqual(date))
    );

  const testInvalid = (formats: string[]) =>
    formats.forEach((fmt) =>
      it(`parses ${fmt} as invalid`, () => expect(parseDate(fmt)).toBeNull())
    );

  // Unambiguous dates w/ year
  testValid(toDate(2000, 1, 23), [
    "23 January 2000",
    "23 Jan 2000",
    "Jan 23 2000",
    "January 23, 2000",
    "23rd Jan 2000",
    "2000 1 23",
    "2000 23 1",
    "1 23 2000",
    "23 01 2000",
    "2000-01-23",
    "2000-1-23",
  ]);

  // Unambiguous dates w/o year
  testValid(toDate(9999, 1, 23), [
    "Jan 23",
    "23 Jan",
    "January 23rd",
    "23 January",
    "01 23",
    "23 1",
  ]);

  // Ambiguous dates w/ year
  testValid(toDate(2000, 11, 11), ["11 11 2000", "2000/11/11"]);

  // Ambiguous dates w/o year
  testValid(toDate(9999, 2, 2), ["2 02", "02 2"]);

  // Ambiguous dates and other invalid input
  testInvalid([
    "2 3",
    "04/05",
    "06 07 2000",
    "13/13/2000",
    "2000-35-3",
    "Shrek",
  ]);
});

describe("/birthdays set command", () => {
  const createOption = (name: string, value: string) => ({
    name,
    value,
    type: 3,
  });
  const createNameOption = (name: string) => createOption("name", name);
  const createDateOption = (date: string) => createOption("date", date);

  const getUpsertedRecord = async (cmd: CommandInteraction) => {
    const data = await Birthday.findOne({
      where: {
        guildId: cmd.guildId,
        userId: cmd.user.id,
        honoree: cmd.options.getString("name"),
      },
    });
    return data?.get();
  };

  beforeAll(async () => await init("sqlite::memory", { logging: false }));
  beforeEach(async () => await Birthday.sync({ force: true }));

  it("replies with an error if name is missing", async () => {
    const cmd = MockCommandInteraction.fromOptions([
      createDateOption("January 23"),
    ]);
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: "Please specify a name.",
    });
    expect(await Birthday.count()).toBe(0);
  });

  it("replies with an error if date is missing", async () => {
    const cmd = MockCommandInteraction.fromOptions([
      createNameOption("Barbara"),
    ]);
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content: "Please specify a date.",
    });
    expect(await Birthday.count()).toBe(0);
  });

  it("replies with an error if date is invalid", async () => {
    const cmd = MockCommandInteraction.fromOptions([
      createNameOption("Barbara"),
      createDateOption("Invalid Date"),
    ]);
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content:
        "Could not parse `Invalid Date` into a valid date. Please try a different date format.",
    });
    expect(await Birthday.count()).toBe(0);
  });

  it("updates the database when given a valid date", async () => {
    const cmd = MockCommandInteraction.fromOptions([
      createNameOption("Barbara"),
      createDateOption("January 23, 2000"),
    ]);
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content:
        "Now observing the anniversary of the birth of Barbara on `2000-01-23`.",
    });
    expect(await Birthday.count()).toBe(1);
    expect(await getUpsertedRecord(cmd)).toEqual({
      id: 1,
      guildId: cmd.guildId,
      userId: cmd.user.id,
      honoree: "Barbara",
      birthdate: "2000-01-23",
      hideYear: false,
    });
  });

  it("updates the database when given a valid date with no year", async () => {
    const cmd = MockCommandInteraction.fromOptions([
      createNameOption("Barbara"),
      createDateOption("23 Jan"),
    ]);
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content:
        "Now observing the anniversary of the birth of Barbara on January 23rd.",
    });
    expect(await Birthday.count()).toBe(1);
    expect(await getUpsertedRecord(cmd)).toEqual({
      id: 1,
      guildId: cmd.guildId,
      userId: cmd.user.id,
      honoree: "Barbara",
      birthdate: "9999-01-23",
      hideYear: true,
    });
  });

  it("replaces an existing record with the same name", async () => {
    const cmd = MockCommandInteraction.fromOptions([
      createNameOption("Barbara"),
      createDateOption("23 Jan"),
    ]);
    await Birthday.upsert({
      guildId: cmd.guildId,
      userId: cmd.user.id,
      honoree: "Barbara",
      birthdate: toDate(9999, 1, 23),
      hideYear: true,
    });
    await execute(cmd);
    expect(cmd.reply).toHaveBeenCalledWith({
      ephemeral: true,
      content:
        "Now observing the anniversary of the birth of Barbara on January 23rd.",
    });
    expect(await Birthday.count()).toBe(1);
    expect(await getUpsertedRecord(cmd)).toEqual({
      id: 1,
      guildId: cmd.guildId,
      userId: cmd.user.id,
      honoree: "Barbara",
      birthdate: "9999-01-23",
      hideYear: true,
    });
  });

  it("replies with an error when something goes wrong", async () => {
    const upsertSpy = jest
      .spyOn(Birthday, "upsert")
      .mockRejectedValue(new Error("It didn't work."));
    try {
      const cmd = MockCommandInteraction.fromOptions([
        createNameOption("Barbara"),
        createDateOption("01 23"),
      ]);
      await execute(cmd);
      expect(cmd.reply).toHaveBeenCalledWith({
        ephemeral: true,
        content: "Something went wrong!",
      });
      expect(await Birthday.count()).toBe(0);
    } finally {
      upsertSpy.mockRestore();
    }
  });
});
