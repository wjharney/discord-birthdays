import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { format, getYear, isValid, parse } from "date-fns";
import { Birthday } from "../../../db";

export const data = new SlashCommandSubcommandBuilder()
  .setName("set")
  .setDescription("Add or update a birthday reminder")
  .addStringOption((opt) =>
    opt
      .setName("name")
      .setDescription("Whose birthday is it?")
      .setRequired(true)
  )
  .addStringOption((opt) =>
    opt
      .setName("date")
      .setDescription("When is the birthday?")
      .setRequired(true)
  );

const parseYMD = (
  year: string | number,
  month: string | number,
  day: string | number,
  format = "yyyy MM dd"
): Date | null => {
  const parsed = parse(`${year} ${month} ${day}`, format, new Date(0));
  return isValid(parsed) ? parsed : null;
};

export const parseDate = (raw: string): Date | null => {
  // Year can be left unspecified, defaults to 9999
  const year = /\d{4}/.exec(raw)?.[0] ?? "9999";
  // Check for month as jan/feb/etc.
  const strMonth = /[a-z]{3}/i.exec(raw)?.[0]?.toLowerCase();
  if (strMonth) {
    const day = /\b(\d\d?)(?:st|nd|rd|th)?\b/.exec(raw)?.[1];
    if (!day) return null;
    return parseYMD(year, strMonth, day, "yyyy MMM d");
  }
  // Month is not a string - probably a number
  // ISO-ish: yyyy-MM-dd, but with arbitrary separator
  const isoish = /\b(\d{4})\D+(\d{1,2})\D+(\d{1,2})\b/.exec(raw);
  if (isoish) {
    const year = isoish[1];
    const month = isoish[2];
    const day = isoish[3];
    if (!year || !month || !day) {
      throw new Error("Huh? ISO-ish date parse RegExp is broken.");
    }
    const parsed = parseYMD(year, month, day);
    if (parsed) return parsed;
  }
  // Expected formats: MM dd yyyy, dd MM yyyy, MM dd, dd MM, yyyy dd MM (dd > 12)
  // Unexpected, but possible: dd YYYY MM, MM YYYY dd

  // There should be two 1- or 2-digit numbers: month and day in unknown order
  const numbers = raw.match(/\b\d\d?\b/g)?.map(Number);
  if (!numbers || numbers.length !== 2) {
    // Too few or too many numbers
    return null;
  }
  const [first, second] = numbers;
  if (!first || !second || first > 31 || second > 31) {
    // Input is not in the range 1-31
    return null;
  } else if (first === second && first <= 12) {
    // If they're the same, order doesn't matter
    return parseYMD(year, first, second);
  } else if (first <= 12 && second > 12) {
    // Format is MM dd
    return parseYMD(year, first, second);
  } else if (first > 12 && second <= 12) {
    // Format is dd MM
    return parseYMD(year, second, first);
  }
  // first and second are either both <= 12 or both > 12
  // In the first case, we can't tell which is which
  // In the second case, it's invalid anyway
  return null;
};

export const execute = async (cmd: CommandInteraction): Promise<void> => {
  const honoree = cmd.options.getString("name");
  if (!honoree) {
    return await cmd.reply({
      ephemeral: true,
      content: "Please specify a name.",
    });
  }
  const rawDate = cmd.options.getString("date");
  if (!rawDate) {
    return await cmd.reply({
      ephemeral: true,
      content: "Please specify a date.",
    });
  }
  const birthdate = parseDate(rawDate.trim());
  if (!birthdate) {
    return await cmd.reply({
      ephemeral: true,
      content: `Could not parse \`${rawDate}\` into a valid date. Please try a different date format.`,
    });
  }
  const hideYear = getYear(birthdate) === 9999;

  try {
    await Birthday.upsert({
      guildId: cmd.guildId,
      userId: cmd.user.id,
      honoree,
      birthdate,
      hideYear,
    });
  } catch (err) {
    return await cmd.reply({
      ephemeral: true,
      content: "Something went wrong!",
    });
  }

  await cmd.reply({
    ephemeral: true,
    content: `Now observing the anniversary of the birth of ${honoree} on ${format(
      birthdate,
      hideYear ? "MMMM do" : "`yyyy-MM-dd`"
    )}.`,
  });
};
