import { BirthdaysListData, Group } from "./config";

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

// dateFns.format(date, 'y MMM') doesn't handle UTC, so just implement it ourselves...
// See: https://github.com/date-fns/date-fns/issues/376
const format = (date: Date) =>
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  `${date.getUTCDate()} ${months[date.getUTCMonth()]!}`;

export function formatListResponse({
  birthdays,
  pagination,
  group,
}: BirthdaysListData): string {
  if (birthdays.length === 0) {
    return "No birthdays to list.";
  }
  const list = birthdays
    .map((bday) => {
      const attrs = bday.get();
      let str = `- *${attrs.honoree}* - ${format(attrs.birthdate)}`;
      if (group === Group.all) str += ` (<@${attrs.userId}>)`;
      return str;
    })
    .join("\n");
  return pagination.max === 1
    ? list
    : `${list}\n(page ${pagination.current} / ${pagination.max})`;
}
