import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import logger from "loglevel";

import { Group, Sort } from "./config";
import { formatListResponse } from "./format";
import { getBirthdaysList } from "./query";
import { isEnumValue } from "../../../../util";

export const data = new SlashCommandSubcommandBuilder()
  .setName("list")
  .setDescription("View a list of birthdays")
  .addStringOption((opt) =>
    opt
      .setName("group")
      .setDescription("Which group of names to view")
      .setRequired(true)
      .addChoices(Object.entries(Group))
  )
  .addStringOption((opt) =>
    opt
      .setName("sort")
      .setDescription("The order in which results should be displayed")
      .setRequired(false)
      .addChoices(Object.entries(Sort))
  )
  .addNumberOption((opt) =>
    opt
      .setName("page")
      .setDescription("Which page of results to return")
      .setRequired(false)
  );

export const execute = async (cmd: CommandInteraction): Promise<void> => {
  const group = cmd.options.getString("group");
  if (!group || !isEnumValue(group, Group)) {
    return await cmd.reply({
      ephemeral: true,
      content: `Unknown group: ${String(group)}`,
    });
  }
  const sort = cmd.options.getString("sort");
  if (!sort || !isEnumValue(sort, Sort)) {
    return await cmd.reply({
      ephemeral: true,
      content: `Unknown sort: ${String(sort)}`,
    });
  }
  const page = cmd.options.getNumber("page") ?? 1;

  let listData;
  try {
    listData = await getBirthdaysList(cmd, { group, sort, page });
  } catch (err) {
    logger.error("Failed to query birthdays", err);
    return await cmd.reply({
      ephemeral: true,
      content: `Something broke when getting the list of birthdays.`,
    });
  }
  return await cmd.reply({
    ephemeral: true,
    content: formatListResponse(listData),
  });
};
