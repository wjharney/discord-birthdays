import { CommandInteraction } from "discord.js";
import { literal, Order, WhereOptions } from "sequelize";
import { Birthday } from "../../../../db";
import { BirthdaysListData, Group, PAGINATION_LIMIT, Sort } from "./config";

if ("birthdate" in Birthday.attributes === false) {
  throw new Error("`birthdate` is no longer a column? Need to fix ORDER BY!");
}

// Use a constant because "``" is easy to mess up
const BIRTHDATE_COL = "`birthdate`";

const extract = (what: string, where: string) =>
  `EXTRACT(${what} from ${where})`;

const normalize = (date: string) =>
  `(100 * ${extract("MONTH", date)} + ${extract("DAY", date)})`;

const BASE_ORDER_BY: Order = [
  "hideYear",
  literal(extract("YEAR", BIRTHDATE_COL)),
  "userId",
  "honoree",
];

const ORDER_BY_CALENDAR = [
  literal(extract("MONTH", BIRTHDATE_COL)),
  literal(extract("DAY", BIRTHDATE_COL)),
  ...BASE_ORDER_BY,
];

const ORDER_BY_UPCOMING = [
  literal(
    `${normalize(BIRTHDATE_COL)} >= ${normalize("CURRENT_DATE")}
    then ${normalize(BIRTHDATE_COL)}
    else ${normalize(BIRTHDATE_COL)} + interval \`1 year\``
  ),
  ...BASE_ORDER_BY,
];

interface ListCommandArguments {
  group: Group;
  page: number;
  sort: Sort;
}

export function getWhereClause(
  cmd: CommandInteraction,
  { group }: ListCommandArguments
): WhereOptions<Birthday["_attributes"]> {
  switch (group) {
    case Group.all:
      return { guildId: cmd.guildId };
    case Group.mine:
      return { guildId: cmd.guildId, userId: cmd.user.id };
    default:
      // Unexpected - calling code should have already handled this case
      throw new Error(`Unknown group: ${String(group)}`);
  }
}

export function getOrderByClause({ sort }: ListCommandArguments): Order {
  switch (sort) {
    case Sort.calendar:
      return Array.from(ORDER_BY_CALENDAR);
    case Sort.upcoming:
      return Array.from(ORDER_BY_UPCOMING);
    default:
      // Unexpected - calling code should have already handled this case
      throw new Error(`Unknown sort: ${String(sort)}`);
  }
}

export async function getBirthdaysList(
  cmd: CommandInteraction,
  args: ListCommandArguments
): Promise<BirthdaysListData> {
  const where = getWhereClause(cmd, args);
  const count = await Birthday.count({ where });
  const pages = Math.ceil(count / PAGINATION_LIMIT);
  const page = Math.max(1, Math.min(args.page, pages));

  const birthdays = await Birthday.findAll({
    where,
    offset: PAGINATION_LIMIT * (page - 1),
    limit: PAGINATION_LIMIT,
    order: getOrderByClause(args),
  });

  return {
    birthdays,
    group: args.group,
    pagination: {
      current: page,
      max: pages,
    },
  };
}
