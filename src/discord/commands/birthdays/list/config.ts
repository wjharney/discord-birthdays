import type { Birthday } from "../../../../db";

export const PAGINATION_LIMIT = 10; // This could be an env var, but ehh...

export enum Group {
  mine = "mine",
  all = "all",
}

export enum Sort {
  calendar = "calendar",
  upcoming = "upcoming",
}

export interface BirthdaysListData {
  birthdays: Birthday[];
  group: Group;
  pagination: {
    current: number;
    max: number;
  };
}
