import type { Birthday } from "../../../../db";
import { BirthdaysListData, Group } from "./config";
import { formatListResponse } from "./format";

const SINGLE_PAGE: BirthdaysListData["pagination"] = {
  current: 1,
  max: 1,
};

const ABE: Birthday["_attributes"] = {
  honoree: "Abraham Lincoln",
  birthdate: new Date("1809-02-12"),
  hideYear: false,
  guildId: "guild",
  userId: "user",
};
// The formatter takes Birthdays, but it only uses them to get the attributes
const BIRTHDAYS = [ABE].map((attrs) => ({ get: () => attrs } as Birthday));

describe("Birthdays list formatter", () => {
  it("returns a special case message when list is empty", () => {
    const message = formatListResponse({
      birthdays: [],
      pagination: SINGLE_PAGE,
      group: Group.all,
    });
    expect(message).toBe("No birthdays to list.");
  });

  it("includes the person's name and birthdate", () => {
    const message = formatListResponse({
      birthdays: BIRTHDAYS,
      pagination: SINGLE_PAGE,
      group: Group.all,
    });
    expect(message).toMatch("Abraham Lincoln");
    expect(message).toMatch("12 February");
  });

  it("includes @mention when listing all birthdays", () => {
    const message = formatListResponse({
      birthdays: BIRTHDAYS,
      pagination: SINGLE_PAGE,
      group: Group.all,
    });
    expect(message).toMatch("<@user>");
  });

  it("excludes @mention when listing own birthdays", () => {
    const message = formatListResponse({
      birthdays: BIRTHDAYS,
      pagination: SINGLE_PAGE,
      group: Group.mine,
    });
    expect(message).not.toMatch("<@user>");
  });

  it("includes pagination information for multiple pages", () => {
    const message = formatListResponse({
      birthdays: BIRTHDAYS,
      pagination: {
        current: 2,
        max: 3,
      },
      group: Group.mine,
    });
    expect(message).toMatch("2 / 3");
  });

  it("excludes pagination information for just 1 page", () => {
    const message = formatListResponse({
      birthdays: BIRTHDAYS,
      pagination: SINGLE_PAGE,
      group: Group.mine,
    });
    expect(message).not.toMatch(/\d+ \/ \d+/);
  });
});
