import {
  SlashCommandBuilder,
  SlashCommandSubcommandBuilder,
} from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { CommandData, createHandlerMap } from "../handler";
import { route } from "../router";

import * as list from "./list";
import * as remove from "./remove";
import * as set from "./set";

export const subcommands: CommandData<SlashCommandSubcommandBuilder>[] = [
  list,
  remove,
  set,
];

export const data = new SlashCommandBuilder()
  .setName("birthdays")
  .setDescription("Manage your birthday reminders");
subcommands.forEach((subcommand) => data.addSubcommand(subcommand.data));

const handlers = createHandlerMap(subcommands);

/** Base command handler for /birthdays */
export const execute = async (cmd: CommandInteraction): Promise<void> =>
  await route(cmd, handlers);
