import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import logger from "loglevel";

export const data = new SlashCommandSubcommandBuilder()
  .setName("remove")
  .setDescription("Remove a birthday from your reminders")
  .addStringOption((opt) =>
    opt.setName("name").setDescription("Person to remove").setRequired(true)
  );

// remove <name>
export const execute = async (cmd: CommandInteraction): Promise<void> => {
  logger.info(data.name, cmd);
  await cmd.reply({
    ephemeral: true,
    content: `Subcommand \`${data.name}\` has not yet been implemented.`,
  });
};
