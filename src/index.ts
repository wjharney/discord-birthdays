import dotenv from "dotenv";
import logger, { LogLevel } from "loglevel";

import * as db from "./db";
import { client } from "./discord";
import { ensureEnv } from "./util";

dotenv.config();

if (process.env["LOGLEVEL"] && process.env["LOGLEVEL"] in logger.levels) {
  logger.setLevel(logger.levels[process.env["LOGLEVEL"] as keyof LogLevel]);
} else {
  logger.setLevel(logger.levels.INFO);
}

(async () => {
  ensureEnv(process.env, "DISCORD_BOT_TOKEN");
  ensureEnv(process.env, "DATABASE_URL");
  await db.init(process.env.DATABASE_URL);
  await client.login(process.env.DISCORD_BOT_TOKEN);
})().catch((err) => {
  logger.error(err);
  process.exit(1);
});
