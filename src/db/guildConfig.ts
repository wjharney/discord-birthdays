import { Model, DataTypes, ModelAttributes } from "sequelize";
import * as CustomTypes from "./types";

export interface GuildConfigAttributes {
  /** Guild ID */
  guildId: string;
  /** Time of day to post birthday reminders. */
  birthdayReminderTime?: string;
  /** Timezone to calculate the right time of day. */
  birthdayReminderTimezone?: number;
  /** Channel ID where birthday reminders should be posted. */
  birthdayReminderChannel?: string;
  /** Whether or not to post birthday reminders at all. */
  birthdayRemindersEnabled?: boolean;
}

/**
 * Command configuration options that are set at the guild level.
 */
export class GuildConfig extends Model<GuildConfigAttributes> {
  static readonly attributes: ModelAttributes<
    GuildConfig,
    GuildConfigAttributes
  > = {
    guildId: {
      type: CustomTypes.SNOWFLAKE,
      allowNull: false,
      primaryKey: true,
    },
    birthdayReminderTime: {
      // Pretty sure this isn't the best way to store a time of day...
      type: DataTypes.STRING(5),
      defaultValue: "12:00",
      // Must match HH:MM
      validate: { is: /^(?:[01]\d|2[0-3]):[0-5]\d$/ },
    },
    birthdayReminderTimezone: {
      type: DataTypes.TINYINT,
      defaultValue: 0,
    },
    birthdayReminderChannel: {
      type: CustomTypes.SNOWFLAKE,
    },
    birthdayRemindersEnabled: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
  };
}
