import { Model, DataTypes, ModelAttributes } from "sequelize";
import * as CustomTypes from "./types";

export interface BirthdayAttributes {
  /** Guild where the command is being used */
  guildId: string;
  /** User who sent the command */
  userId: string;
  /** Name of the person whose birthday is being reminded about */
  honoree: string;
  /** Date of the honoree's birthday */
  birthdate: Date;
  /** Whether the reminder should include the honoree's age */
  hideYear: boolean;
}

/**
 * Configuration options for the /birthday command
 */
export class Birthday extends Model<BirthdayAttributes> {
  static readonly attributes: ModelAttributes<Birthday, BirthdayAttributes> = {
    guildId: {
      type: CustomTypes.SNOWFLAKE,
      allowNull: false,
      unique: "guild-user-honoree",
    },
    userId: {
      type: CustomTypes.SNOWFLAKE,
      allowNull: false,
      unique: "guild-user-honoree",
    },
    honoree: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: "guild-user-honoree",
    },
    birthdate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    hideYear: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  };
}
