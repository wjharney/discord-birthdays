import { Birthday, BirthdayAttributes } from "./birthday";
import { init } from "./init";

describe("Birthdays table", () => {
  beforeAll(async () => {
    await init("sqlite::memory", { logging: false });
    await Birthday.sync({ force: true });
  });
  it("is unique per guild/user/honoree", async () => {
    const base: BirthdayAttributes = {
      guildId: "GUILD",
      userId: "USER",
      honoree: "HONOREE",
      birthdate: new Date("2000-01-23"),
      hideYear: false,
    };
    await Birthday.upsert(base);
    await Birthday.upsert({ ...base, guildId: "OTHER-GUILD" });
    await Birthday.upsert({ ...base, userId: "OTHER-USER" });
    await Birthday.upsert({ ...base, honoree: "OTHER-HONOREE" });
    await Birthday.upsert({
      ...base,
      birthdate: new Date("9999-01-23"),
      hideYear: true,
    });
    expect(await Birthday.count()).toBe(4);
  });
});
