import { DataTypes } from "sequelize";

// Used by Discord for some IDs. 64-bit int as decimal string has max 20 chars.
export const SNOWFLAKE = DataTypes.STRING(20);
