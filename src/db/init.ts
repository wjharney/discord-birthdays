import { Options, Sequelize } from "sequelize";
import logger from "loglevel";

import { Birthday } from "./birthday";
import { GuildConfig } from "./guildConfig";

/**
 * Initialize the database
 * @param connection Database connection string
 * @param options Sequelize constructor options
 * @returns Initialized sequelize object
 */
export const init = async (
  connection: string,
  options: Options = {}
): Promise<Sequelize> => {
  const sequelize = new Sequelize(connection, options);
  Birthday.init(Birthday.attributes, { sequelize, timestamps: false });
  GuildConfig.init(GuildConfig.attributes, { sequelize, timestamps: false });
  await sequelize.sync();
  logger.info("Connected to database.");
  return sequelize;
};
