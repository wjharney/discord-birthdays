/* eslint-env node */
module.exports = {
  "package.json": "yarn check:package",
  "*.ts": () => [
    "tsc -p tsconfig.json --noEmit",
    "yarn check:style",
    "yarn test",
  ],
};
